# Ccalls.jl

This package provides the `Ccall` function as a much more flexible interface to
[foreign function calls](https://docs.julialang.org/en/v1/manual/calling-c-and-fortran-code/)
than Julia’s standard `ccall` and `@ccall`.
`Ccall` is a standard Julia function and thus behaves like any other Julia
function, while still supporting all features of `ccall` and `@ccall`.
In particular, this makes it possible to pass any kind of expression as
arguments (including the
[“splat” operation using `...`](https://docs.julialang.org/en/v1/base/base/#...)).


# Example usage

```julia
julia> using Ccalls

```


## `@ccall`-like syntax

```julia
julia> Ccall(:rand, Cint)
1739233194

julia> Ccall(:pow, Cdouble, 2 => Cdouble, 10 => Cdouble)
1024.0

# varargs arguments are introduced using … (\dots)
julia> Ccall(
           :printf, Cint, "%.3f %d %s\n" => Cstring, …,
           2 => Cdouble, 10 => Cint, "xyz" => Cstring
       )
2.000 10 xyz
13

```


## `ccall`-like syntax
```julia
julia> Ccall(:rand, Cint, ())
808583754

julia> Ccall(:pow, Cdouble, (Cdouble, Cdouble), 2, 10)
1024.0


# varargs arguments are introduced using … (\dots)
julia> Ccall(
           :printf, Cint, (Cstring, …, Cdouble, Cint, Cstring),
           "%.3f %d %s\n", 2, 10, "xyz"
       )
2.000 10 xyz
13

```


## Splatting works!
```julia
julia> args = (2, 10)

julia> Ccall(:pow, Cdouble, (Cdouble, Cdouble), args...)
1024.0


```


## Specifying the library
```julia
julia> Ccall((:pow, "libm.so.6"), Cdouble, 2 => Cdouble, 10 => Cdouble)
1024.0

```


## Using a function pointer
```julia
julia> using Base.Libc.Libdl

julia> libm = dlopen("libm.so.6")
Ptr{Nothing} @0x0000000001ba3c90

julia> pow = dlsym(libm, :pow)
Ptr{Nothing} @0x00007f6208726ed0

julia> Ccall(pow, Cdouble, 2 => Cdouble, 10 => Cdouble)
1024.0

```


## Specifying a calling convention
```julia
julia> Ccall(:pow, Cdouble, 2 => Cdouble, 10 => Cdouble; convention=:cdecl)
1024.0


```


# Acknowledgements

[Zeke (uniment)](https://discourse.julialang.org/u/uniment) had the original
idea of using `@generated` functions to avoid the limitations of the existing
foreign call interfaces in Julia (`ccall`, `@ccall`).
([Original discussion on the Julia Discourse.](https://discourse.julialang.org/t/is-it-possible-to-splat-into-ccall/95761))
