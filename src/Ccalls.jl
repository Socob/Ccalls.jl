module Ccalls

using Base.Meta

export Ccall, …, ..


struct Ellipsis end

"""
An argument of … in the type signature passed to `Ccall` marks the start of
varargs arguments to the called function.
"""
const … = Ellipsis()
"""
Alias for ….
"""
const .. = …

const DEFAULT_CONVENTION = :ccall
const FOREIGNCALL_CONVENTION_IDX = 5


"""
    Ccall(func, returntype, ...; convention=$(repr(DEFAULT_CONVENTION)))

Call a function in a C-exported shared library, like `@ccall` or `ccall`.

See also: [`@ccall`](@ref), [`ccall`](@ref).
"""
function Ccall end

@generated function Ccall(
	::Val{convention}, ::Val{func}, ::Type{returntype}, ::Type{tup_argtypes},
	args...
) where {convention, func, returntype, tup_argtypes <: Tuple}
	@assert !(func isa Tuple) || length(func) == 2
	argtypes_in = (tup_argtypes.parameters...,)
	varargs_indices = findall(==(…), argtypes_in)
	length(varargs_indices) < 2 || throw(ArgumentError(
		"more than one instance of “…” in function signature"))
	argtypes = filter(!=(…), argtypes_in)
	length(argtypes) == length(args) || throw(ArgumentError(
		"lengths of function signature and arguments must be equal"))
	is_varargs = !isempty(varargs_indices)
	varargs_idx = is_varargs ? only(varargs_indices) : length(args) + 1
	# build generated function body
	func_expr =
		func isa Tuple ? :($(string(func[2])).$(func[1])) :
		func isa Ptr ? Expr(:$, :func) :
		func
	result = :(@ccall $func_expr(;)::$returntype)
	@assert isexpr(result.args[end], :(::), 2)
	call_expr = result.args[end].args[begin]
	@assert isexpr(call_expr, :call, 2)
	ccall_args(indices) = (:(args[$i]::$(argtypes[i])) for i in indices)
	append!(call_expr.args, ccall_args(1:varargs_idx - 1))
	varargs_params = is_varargs ?
		call_expr.args[2] : popat!(call_expr.args, 2)
	@assert isexpr(varargs_params, :parameters, 0)
	if is_varargs
		append!(varargs_params.args, ccall_args(varargs_idx:length(args)))
	end
	if convention ≢ DEFAULT_CONVENTION
		# @ccall currently doesn’t support specifying a calling convention and
		# always uses :(:ccall) (also see discussion here:
		# <https://github.com/JuliaLang/julia/pull/32748>), so “manually” set
		# the calling convention field in the resulting Expr(:foreigncall);
		# see https://docs.julialang.org/en/v1/devdocs/ast/#Expr-types
		# for Expr(:foreigncall) fields
		result = macroexpand(@__MODULE__, result; recursive=false)
		@assert isexpr(result, :block)
		foreigncall = result.args[end]
		@assert isexpr(foreigncall, :foreigncall)
		foreigncall.args[FOREIGNCALL_CONVENTION_IDX] = quot(convention)
	end
	return result
end

Ccall(convention::Val, func::Val, returntype::Type, argtypes, args...) =
	Ccall(convention, func, returntype, Tuple{argtypes...}, args...)

Ccall(
	(function_name, library)::Tuple{Symbol, AbstractString}, returntype::Type,
	argtypes, args...; convention=DEFAULT_CONVENTION
) =
	Ccall(
		Val(convention), Val((function_name, Symbol(library))), returntype,
		argtypes, args...
	)

"""
    Ccall(
        (function_name, library), returntype, (argtype1, ... [, …, varargtype1, ...]),
        argvalue1, ... [, …, varargvalue1, ...]; convention=$(repr(DEFAULT_CONVENTION))
    )
    Ccall(
        function_name, returntype, (argtype1, ... [, …, varargtype1, ...]),
        argvalue1, ... [, …, varargvalue1, ...]; convention=$(repr(DEFAULT_CONVENTION))
    )
    Ccall(
        function_pointer, returntype, (argtype1, ... [, …, varargtype1, ...]),
        argvalue1, ... [, …, varargvalue1, ...]; convention=$(repr(DEFAULT_CONVENTION))
    )

Equivalent to

    ccall((function_name, library), convention, returntype, (argtype1, ...), argvalue1, ...)
    ccall(function_name, convention, returntype, (argtype1, ...), argvalue1, ...)
    ccall(function_pointer, convention, returntype, (argtype1, ...), argvalue1, ...)

Varargs arguments are introduced using `…` (`\\dots`) (varargs arguments with
arbitrary types are not possible with `ccall`).
"""
Ccall(
	func, returntype::Type, argtypes, args...; convention=DEFAULT_CONVENTION
) =
	Ccall(Val(convention), Val(func), returntype, argtypes, args...)

Ccall(
	(function_name, library)::Tuple{Symbol, AbstractString}, returntype::Type,
	args::Union{Pair, Ellipsis}...; convention=DEFAULT_CONVENTION
) =
	Ccall((function_name, Symbol(library)), returntype, args...; convention)

"""
    Ccall(
        (function_name, library), returntype,
        argvalue1 => argtype1, ... [, …, varargvalue1 => varargtype1, ...]; convention=$(repr(DEFAULT_CONVENTION)
    )
    Ccall(
        function_name, returntype,
        argvalue1 => argtype1, ... [, …, varargvalue1 => varargtype1, ...]; convention=$(repr(DEFAULT_CONVENTION))
    )
    Ccall(
        function_pointer, returntype,
        argvalue1 => argtype1, ... [, …, varargvalue1 => varargtype1, ...]; convention=$(repr(DEFAULT_CONVENTION))
    )

Equivalent to

    @ccall library.function_name(argvalue1::argtype1, ...; varargvalue1::varargtype1, ...)
    @ccall function_name(argvalue1::argtype1, ...; varargvalue1::varargtype1, ...)
    @ccall \$function_pointer(argvalue1::argtype1, ...; varargvalue1::varargtype1, ...)

Varargs arguments are introduced using `…` (`\\dots`).
If `convention` is given: additionally specifies the calling convention (this is
not possible with `@ccall`).
"""
function Ccall(
	func, returntype::Type, args::Union{Pair, Ellipsis}...;
	convention=DEFAULT_CONVENTION
)
	argtypes = (((x ≡ …) ? x : last(x) for x in args)...,)
	argvals = ((first(x) for x in args if x ≢ …)...,)
	return Ccall(func, returntype, argtypes, argvals...; convention)
end


end
