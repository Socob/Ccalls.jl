using Ccalls
using Base.Libc.Libdl
using Test


function cstring_to_string(bytes)
	nul_idx = findfirst(==(0), bytes)
	@assert !isnothing(nul_idx)
	return String(bytes[begin:nul_idx - 1])
end


@testset "Ccalls" begin
	libm = dlopen("libm.so.6")
	pow_ptr = dlsym(libm, :pow)

	# no arguments
	for rand in (:rand, (:rand, "libc.so.6")),
			kwarg in ((;), (convention=:cdecl,))
		@test Ccall(rand, Cint, (); kwarg...) ≥ 0
		@test Ccall(rand, Cint; kwarg...) ≥ 0
	end

	# without varargs, 1 argument
	for abs in (:abs, (:abs, "libc.so.6")),
			kwarg in ((;), (convention=:cdecl,))
		@test Ccall(abs, Cint, (Cint,), -3; kwarg...) == 3
		@test Ccall(abs, Cint, -3 => Cint; kwarg...) == 3
	end

	# without varargs, 2 arguments
	for pow in (:pow, (:pow, "libm.so.6"), pow_ptr),
			kwarg in ((;), (convention=:cdecl,))
		@test Ccall(pow, Cdouble, (Cdouble, Cdouble), 2, 10; kwarg...) == 1024.
		@test Ccall(
			pow, Cdouble, 2 => Cdouble, 10 => Cdouble; kwarg...
		) == 1024.
		@test Ccall(
			pow, Cdouble, 3 => Cdouble, 3 => Cdouble; kwarg...
		) == 27.
	end

	# with varargs
	buffer = Vector{UInt8}(undef, 100)
	for sprintf in (:sprintf, (:sprintf, "libc.so.6")),
			kwarg in ((;), (convention=:cdecl,))
		# ccall-like interface
		@test Ccall(
			sprintf, Cint, (Ref{UInt8}, Cstring, …, Cdouble, Cint, Cstring),
			buffer, "%.3f %d %s\n", 2, 10, "xyz"; kwarg...
		) == 13
		@test cstring_to_string(buffer) == "2.000 10 xyz\n"
		@test Ccall(
			sprintf, Cint, (Ref{UInt8}, Cstring, …, Cint), buffer, "%d\n", 777;
			kwarg...
		) == 4
		@test cstring_to_string(buffer) == "777\n"
		@test Ccall(
			sprintf, Cint, (Ref{UInt8}, Cstring, …), buffer, "test\n"; kwarg...
		) == 5
		@test cstring_to_string(buffer) == "test\n"
		# @ccall-like interface
		@test Ccall(
			sprintf, Cint, buffer => Ref{UInt8}, "%.3f %d %s\n" => Cstring, …,
			2 => Cdouble, 10 => Cint, "xyz" => Cstring;
			kwarg...
		) == 13
		@test cstring_to_string(buffer) == "2.000 10 xyz\n"
		@test Ccall(
			sprintf, Cint, buffer => Ref{UInt8}, "%d\n" => Cstring, …,
			777 => Cint;
			kwarg...
		) == 4
		@test cstring_to_string(buffer) == "777\n"
		@test Ccall(
			sprintf, Cint, buffer => Ref{UInt8}, "test\n" => Cstring, …;
			kwarg...
		) == 5
		@test cstring_to_string(buffer) == "test\n"
	end

	# error cases
	@test_throws ArgumentError Ccall(
		:pow, Cdouble, (Cdouble, Cdouble, Cstring), 2, 10)
	# TODO: should these cases throw ArgumentError instead?
	@test_throws LoadError Ccall(:rand, Cint, (…,))
	@test_throws LoadError Ccall(
		:sprintf, Cint, (…, Ref{UInt8}, Cstring), buffer, "test\n"
	)
	@test_throws ArgumentError Ccall(
		:sprintf, Cint, (Ref{UInt8}, Cstring, …, Cdouble, Cint, …, Cstring),
		buffer, "%.3f %d %s\n", 2, 10, "xyz"
	)
	@test_throws ArgumentError Ccall(
		:sprintf, Cint, buffer => Ref{UInt8}, "%.3f %d %s\n" => Cstring, …,
		2 => Cdouble, 10 => Cint, …, "xyz" => Cstring
	)
end
